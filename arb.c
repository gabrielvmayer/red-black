#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
} ArvoreRB;

int eh_no_vermelho(ArvoreRB * no){
  if(!no) return BLACK;
  return(no->cor == RED);
}

int buscar (ArvoreRB *a, int v) {
  if (a == NULL) { return 0; } /*Nao achou*/
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } /*Achou*/
}

void in_order(ArvoreRB *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

void print(ArvoreRB * a,int spaces){
  int i;
  for(i=0;i<spaces;i++) printf(" ");
  if(!a){
    printf("//\n");
    return;
  }

  printf("%d\n", a->info);
  print(a->esq,spaces+2);
  print(a->dir,spaces+2);
}

//

int verificar_arv_vazia(ArvoreRB *a) {
  return (a == NULL);
}

ArvoreRB *rotacionar_arv_direita(ArvoreRB *no) {
  ArvoreRB *arv = no->esq;
  no->esq = arv->dir;
  arv->dir = no;
  arv->cor = no->cor;
  no->cor = RED;
  
  return arv;
}

ArvoreRB *rotacionar_arv_esquerda(ArvoreRB *no) {
  ArvoreRB *arv = no->dir;
  no->dir = arv->esq;
  arv->esq = no;
  arv->cor = no->cor;
  no->cor = RED;

  return arv;
}

void alterar_cor(ArvoreRB *no) {
  no->cor = RED;
  no->esq->cor = BLACK;
  no->dir->cor = BLACK;
}

ArvoreRB *inserir(ArvoreRB *a, int v) {
  bool troca = false;

  if (a == NULL) {
    a = (ArvoreRB *)malloc(sizeof(ArvoreRB));
    a->info = v;
    a->cor = BLACK;
    a->esq = a->dir = NULL;
  } else if (v < a->info) {
    troca = a->esq == NULL;
    a->esq = inserir(a->esq, v);
    if (troca)
      a->esq->cor = RED;
  } else {
    troca = a->dir == NULL;
    a->dir = inserir(a->dir, v);
    if (troca)
      a->dir->cor = RED;
  }

  if(eh_no_vermelho(a->dir) && !eh_no_vermelho(a->esq))
    a = rotacionar_arv_esquerda(a);
  else if (eh_no_vermelho(a->esq) && eh_no_vermelho(a->esq->esq))
    a = rotacionar_arv_direita(a);
  else if (eh_no_vermelho(a->dir) && eh_no_vermelho(a->esq))
    alterar_cor(a);

  return a;
}

ArvoreRB *remover(ArvoreRB *a, int x){
  int temp, ehPreto, filhos = 0;
  ArvoreRB *aux_no, *aux_pai;

  if (!a)
    return (NULL);

  if(a->info < x) {
    ehPreto = eh_no_vermelho(a->dir);
    a->dir = remover(a->dir, x);

    if ((ehPreto && eh_no_vermelho(a)) || (!ehPreto && !eh_no_vermelho(a))) {
      if (a->dir == NULL)
        a->cor = BLACK;
      else if (a->dir->esq == NULL && a->dir->dir == NULL)
        a->dir->cor = BLACK;
      
    } else if (ehPreto && !eh_no_vermelho(a)) {
      if (verificar_arv_vazia(a->dir) && verificar_arv_vazia(a->esq))
        a->cor = BLACK;
      else if (!verificar_arv_vazia(a->dir))
        a->dir->cor = BLACK;
    }

  } else if (a->info > x) {
    ehPreto = eh_no_vermelho(a->esq);
    a->esq = remover(a->esq, x);
      
    if ((ehPreto && eh_no_vermelho(a)) || (!ehPreto && !eh_no_vermelho(a))) {
      if (a->esq == NULL)
        a->cor = BLACK;
      else if (a->esq->esq == NULL && a->esq->dir == NULL)
        a->esq->cor = BLACK;
    } else if (ehPreto && !eh_no_vermelho(a)) {
      if (verificar_arv_vazia(a->esq) && verificar_arv_vazia(a->dir))
        a->cor = BLACK;
      else if (!verificar_arv_vazia(a->esq))
        a->esq->cor = BLACK;
    }
  } else {
    if (a->esq)
      filhos++;
    
    if (a->dir)
      filhos++;

    if (filhos == 0) {
      free(a);
      return (NULL);

    } else if (filhos == 1) {
      aux_no = a->esq ? a->esq : a->dir;
      free(a);
      return (aux_no);

    } else {
      aux_no = a->esq;
      aux_pai = a;
        
      while(aux_no->dir){
        aux_pai = aux_no;
        aux_no = aux_no->dir;
      }
        
      temp = a->info;
      a->info = aux_no->info;
      aux_no->info = temp;
      aux_pai->dir = remover(aux_no, temp);
        
      return a;
    }
  }

  if (eh_no_vermelho(a->dir) && !eh_no_vermelho(a->esq))
    a = rotacionar_arv_esquerda(a);
  else if (eh_no_vermelho(a->esq) && !eh_no_vermelho(a->esq->esq))
    a = rotacionar_arv_direita(a);
  else if (eh_no_vermelho(a->dir) && !eh_no_vermelho(a->esq))
    alterar_cor(a);

  return a;
}

ArvoreRB *arv_libera(ArvoreRB *a) {
  if (!verificar_arv_vazia(a)) {
    arv_libera(a->esq);
    arv_libera(a->dir);
    free(a);
  }
  return NULL;
}

int arv_altura(ArvoreRB *a) {
  int altura = 0;
  ArvoreRB *no = a;

  while (no != NULL){
    if(eh_no_vermelho(no) == 0)
      altura++;
      no = no->esq;
  }
  return altura;
}

int main(){
  ArvoreRB * arv;
  arv = inserir(arv, 8);
  arv = inserir(arv, 5);
  arv = inserir(arv, 10);
  arv = inserir(arv, 2);
  arv = inserir(arv, 7);
  arv = inserir(arv, 4);
  arv = inserir(arv, 1);
  arv = inserir(arv, 9);
  arv = inserir(arv, 6);
  arv = inserir(arv, 3);
  print(arv, arv_altura(arv));

  printf("\nRemocao iniciada. Removendo os numeros 5, 1 e 2:\n");
  arv = remover(arv, 5);
  arv = remover(arv, 1);
  arv = remover(arv, 2);
  print(arv, arv_altura(arv));
  arv_libera(arv);

  return 0;
}
